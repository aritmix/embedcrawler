﻿using Abot.Crawler;
using Abot.Poco;
using FaceThePlanet.Crawler.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace FaceThePlanet.Crawler
{
    class Program
    {
        static Object lockObj = new Object();

        static void Main(string[] args)
        {
            File.Delete("results.txt");
            log4net.Config.XmlConfigurator.Configure();

            var uriToCrawl = GetSiteToCrawl();

            uriToCrawl.ForEach(url =>
            {
                new Crawler().StartCrawling(url);
            });

            Console.ReadLine();
        }

        private static List<Uri> GetSiteToCrawl()
        {
            List<Uri> domains = new List<Uri>();
            //\fwdjwplayerandytexample
            string domainsToCrawlValue = ConfigurationManager.AppSettings["domainsToCrawlFromCsvFiles"] as String;
            if (String.IsNullOrWhiteSpace(domainsToCrawlValue))
            {
                System.Console.WriteLine("Please set 'domainsToCrawl' key at app.cpnfig seperater by ';'");
            }

            string CsvFilesBasePathValue = ConfigurationManager.AppSettings["CsvFilesBasePath"] as String;
            if (String.IsNullOrWhiteSpace(CsvFilesBasePathValue))
            {
                System.Console.WriteLine("Please set 'CsvFilesBasePath' key at app.cpnfig");
            }


            var domainsToCrawlFiles = domainsToCrawlValue.Split(new String[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                .Where(file => !String.IsNullOrWhiteSpace(file) && file.EndsWith("csv"))
                .Select(file => file.Trim())
                .ToList();

            foreach (var csvPath in domainsToCrawlFiles)
            {
                var fullCsvPath = Path.Combine(CsvFilesBasePathValue, csvPath);
                if (File.Exists(fullCsvPath))
                {
                    var reader = new StreamReader(File.OpenRead(fullCsvPath));
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        if (values != null && values.Count() > 0)
                        {
                            var host = values[0];

                            if (Uri.IsWellFormedUriString(host, UriKind.RelativeOrAbsolute) &&
                                Uri.CheckHostName(host) != UriHostNameType.Unknown)
                            {
                                if (!host.StartsWith("http"))
                                {
                                    host = "http://" + host;
                                }

                                Uri url = null;

                                if (Uri.TryCreate(host, UriKind.Absolute, out url))
                                {
                                    domains.Add(new Uri(host));
                                }
                            }

                        }
                    }

                }
            }
            return domains;
        }

    }
}
