﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace FaceThePlanet.Crawler.Models
{
    public partial class CrawlerResult
    {
        [XmlElement]
        public string Domain { get; set; }

        [XmlElement]
        public string FullUrl { get; set; }

        [XmlElement]
        public string HtmlElement { get; set; }

        [XmlElement]
        public String HtmlContent { get; set; }

        [XmlElement]
        public Boolean IsIframe { get; set; }
    }

}
