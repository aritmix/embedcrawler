﻿using Abot.Crawler;
using Abot.Poco;
using FaceThePlanet.Crawler.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace FaceThePlanet.Crawler
{
    internal class Crawler
    {
        static Object lockObj = new Object();

        public void StartCrawling(Uri url)
        {
            IWebCrawler crawler;

            //Uncomment only one of the following to see that instance in action
            crawler = GetDefaultWebCrawler();
            //crawler = GetManuallyConfiguredWebCrawler();
            //crawler = GetCustomBehaviorUsingLambdaWebCrawler();

            //Subscribe to any of these asynchronous events, there are also sychronous versions of each.
            //This is where you process data about specific events of the crawl
            crawler.PageCrawlStartingAsync += crawler_ProcessPageCrawlStarting;
            crawler.PageCrawlCompletedAsync += crawler_ProcessPageCrawlCompleted;
            crawler.PageCrawlDisallowedAsync += crawler_PageCrawlDisallowed;
            crawler.PageLinksCrawlDisallowedAsync += crawler_PageLinksCrawlDisallowed;

            //Start the crawl
            //This is a synchronous call
            CrawlResult result = crawler.Crawl(url);
            //result.s
            Console.WriteLine(result.ErrorOccurred);
         

            //Now go view the log.txt file that is in the same directory as this executable. It has
            //all the statements that you were trying to read in the console window :).
            //Not enough data being logged? Change the app.config file's log4net log level from "INFO" TO "DEBUG"

            //PrintDisclaimer();
        }


        private static IWebCrawler GetDefaultWebCrawler()
        {
            return new PoliteWebCrawler();
        }


        private static void PrintDisclaimer()
        {
            //PrintAttentionText("The demo is configured to only crawl a total of 10 pages and will wait 1 second in between http requests. This is to avoid getting you blocked by your isp or the sites you are trying to crawl. You can change these values in the app.config or Abot.Console.exe.config file.");
        }

        private static void PrintAttentionText(string text)
        {
            ConsoleColor originalColor = System.Console.ForegroundColor;
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            System.Console.WriteLine(text);
            System.Console.ForegroundColor = originalColor;
        }

        static void crawler_ProcessPageCrawlStarting(object sender, PageCrawlStartingArgs e)
        {
            //Process data
        }

        static void crawler_ProcessPageCrawlCompleted(object sender, PageCrawlCompletedArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(Parse), e);
        }

        private static void Parse(object state)
        {
            PageCrawlCompletedArgs e = state as PageCrawlCompletedArgs;
            var htmlElementsToSearchForValue = ConfigurationManager.AppSettings["htmlElementsToSearchFor"] as String;
            if (String.IsNullOrWhiteSpace(htmlElementsToSearchForValue))
            {
                return;
            }

            var htmlElementsToSearchFor = htmlElementsToSearchForValue.Split(new String[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                .Where(url => !String.IsNullOrWhiteSpace(url))
                .Select(url => url.Trim())
                .ToList();

            ParseByTags(e, htmlElementsToSearchFor);
            ParseByIFrame(e, htmlElementsToSearchFor);
        }

        private static void ParseByTags(PageCrawlCompletedArgs e, List<string> htmlElementsToSearchFor)
        {
            htmlElementsToSearchFor.ForEach(tag =>
            {
                var desc = e.CrawledPage.HtmlDocument.DocumentNode.Descendants(tag);//.Where(n => n.n(tag));//e.CrawledPage.HtmlDocument.DocumentNode.Descendants(tag);

                if (desc != null)
                {
                    desc.ToList().ForEach(element =>
                    {
                        CrawlerResult crawlerResult = new CrawlerResult();

                        //Set Data
                        crawlerResult.Domain = e.CrawledPage.Uri.Host;
                        crawlerResult.HtmlContent = element.OuterHtml;
                        crawlerResult.HtmlElement = tag;
                        crawlerResult.FullUrl = e.CrawledPage.Uri.AbsoluteUri;
                        crawlerResult.IsIframe = false;

                        AppendToFile(crawlerResult);
                    });
                }
            });
        }

        private static void AppendToFile(CrawlerResult crawlerResult)
        {
            Monitor.Enter(lockObj);
            using (StreamWriter sw = File.AppendText("results.txt"))
            {
                //Create our own namespaces for the output

                //Add an empty namespace and empty value
                //ns.Add("", "");

                //Create the serializer

                //Serialize the object with our own namespaces (notice the overload)

                sw.Write("<CrawlerResult>");
                sw.Write("<Domain>{0}</Domain>", crawlerResult.Domain);
                sw.Write("<FullUrl>{0}</FullUrl>", crawlerResult.FullUrl);
                sw.Write("<HtmlElement>{0}></HtmlElement>", crawlerResult.HtmlElement);
                sw.Write("<HtmlContent><![CDATA[{0}]]></HtmlContent>", crawlerResult.HtmlContent);
                sw.Write("<IsIframe>{0}</IsIframe>", crawlerResult.IsIframe);
                sw.WriteLine("</CrawlerResult>");
            }
            Monitor.Exit(lockObj);
        }

        private static void ParseByIFrame(PageCrawlCompletedArgs e, List<string> htmlElementsToSearchFor)
        {
            var iFrames = e.CrawledPage.HtmlDocument.DocumentNode.Descendants("iframe"); //.Where(n => n.n(tag));//e.CrawledPage.HtmlDocument.DocumentNode.Descendants(tag);
            foreach (var iframe in iFrames)
            {
                Uri url = null;
                    
                if(!Uri.TryCreate(iframe.Attributes["src"].Value, UriKind.Absolute, out url))
                    continue;

                if (!iframe.Attributes["src"].Value.StartsWith("http"))
                    continue;

                HtmlWeb website = new HtmlWeb();
                HtmlDocument rootDocument = website.Load(url.AbsoluteUri);


                htmlElementsToSearchFor.ForEach(tag =>
                {
                    var desc = rootDocument.DocumentNode.Descendants(tag);

                    if (desc != null)
                    {
                        desc.ToList().ForEach(element =>
                        {
                            CrawlerResult crawlerResult = new CrawlerResult();

                            //Set Data
                            crawlerResult.Domain = e.CrawledPage.Uri.Host;
                            crawlerResult.HtmlContent = element.OuterHtml;
                            crawlerResult.HtmlElement = tag;
                            crawlerResult.FullUrl = e.CrawledPage.Uri.AbsoluteUri;
                            crawlerResult.IsIframe = true;

                            AppendToFile(crawlerResult);
                        });
                    }
                });

            }
        }
        static void crawler_PageLinksCrawlDisallowed(object sender, PageLinksCrawlDisallowedArgs e)
        {
            //Process data
        }

        static void crawler_PageCrawlDisallowed(object sender, PageCrawlDisallowedArgs e)
        {
            //Process data
        }
    }
}
